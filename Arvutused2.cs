﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{


    enum Tunnus { Suur=1, Puust=2, Punane= 4, Kolisev= 8}
        enum Mast {Risti, Ruutu, Ärtu, Poti }
    class Program
    {
        static void Main(string[] args)
        {

            Mast m1 = Mast.Ruutu;
            Console.WriteLine((int)m1); //väärtuste loendamine hakkab 0-st, seepärast on ka selle tulem 1
            m1 = (Mast)Enum.Parse(typeof(Mast), "Ärtu");

            int a = 1;
            int b = 2;
            Console.WriteLine(a+b);

            byte c = 1;
            byte d = 2;
            Console.WriteLine(c - d);

            Console.WriteLine(a + c);
            Console.WriteLine(b * d);
            Console.WriteLine($"a={a} b={b} c={c}");

            double e = 1;
            double f = 2;
            Console.WriteLine(e + f);
            Console.WriteLine(a * e);

            Tunnus t1 = Tunnus.Punane | Tunnus.Puust;
            Console.WriteLine(t1);

            t1 = (Tunnus)9;
            Console.WriteLine(t1);

            t1 ^= Tunnus.Punane;
            Console.WriteLine(t1);

            char n= 'n';
            n = "Henn"[2];
            Console.WriteLine(b + 2);

            string jutt = "Õpetaja ütles: \"Tere lapsed\"!"; //kas tagurpidi kaldkriips v topelt jutumärgid
       
            Console.WriteLine(jutt);

            Console.WriteLine("Külli\n Siimon");



         




        }
    }
}
